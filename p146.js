/* PAGE 146

PROCEDURE
La commande « RUN » lance le programme. L'écran propose un menu. On choisit l'un des programmes en appuyant sur les touches de 1 à 8. Après introduction des dates à traiter, on appuie sur la touche « ENTER » pour valider le choix. De même, on revient au menu en appuyant sur la touche « ESPACE ».
L'impression des résultats ou du calendrier complet sur une imprimante est déclenchée en appuyant sur la touche 9.
SIGNIFICATION DES VARIABLES UTILISEES
AC : ACC : Millésime de l'année civile.
AH : AHH : Millésime de l'année hébraïque.
AL:
Année limite.
CH :
Nombre de cycles de Chemita.
COM :
Nombre d'années communes.
CYL :
Nombre de cycles lunaires.
CYS:
Nombre de cycles solaires.
DCTN :
Date civile de la Tékoufa de Nissan.
DHTN :
Date hébraïque de la Tékoufa de Nissan.
EMB :
Nombre d'années embolismiques.
F
Numéro de la fête.
FC (1) à FC (5): Facteur de correction pour les fêtes qui tombent un Chabbat.
FF
FEV
Numéro de la fête ou du jeûne.
:
Nombre de jours de février.
Nombre de jours de 'Hechvan.
HK
HL
HR
HTK
IC
Jours de 'Hechvan + ceux de Kislev.
Nombre de 'halakim.
Nombre d'heures.
:
:
Heure de la Tékoufa.
Intervalle dans le calendrier civil entre le 1er sep tembre et DCTN.
1h:
Intervalle dans le calendrier hébraïque entre Roch-Hachana et DHTN.
ICH:
Intervalle entre le 1er septembre et Roch-Hachana.
IHC
:
Intervalle entre Roch-Hachana et le 1er septembre.
IP :
Périphérique adressé :
9 = imprimante, <> 9 = écran.
Jour de la semaine.
JAC
JAH :
:
Nombre de jours de l'année civile.
Nombre de jours de l'année hébraïque.




*/
