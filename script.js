/*PAGE 147

JO:
Premier jour du mois.
JRS
:
Jour de la semaine du retard solaire.
JTKN:
Jour de la semaine de la Tékoufa de Nissan.
Nombre de jours de Kislev.
MC : MCC :Mois civils.
MH : MHH : MX :
Mois hébraïques.
ML
Mois limite.
MN
Nombre de minutes.
MO
:
Mois initial.
MOIS :
Nombre de mois de l'année juive.
MOLAD :
Molad après élimination des semaines entières.
MOLNI :
Molad de Nissan.
MOLTI :
Molad de Tichri.
MS :
Mois supplémentaire de l'année juive.
NIS
Jour de Roch-'Hodech Nissan ou celui de Pessa'h.
NS
Nombre de semaines écoulées depuis Sim'hat-Torah.
PJ (1) à PJ (7): Parachiyot jumelées.
QC :QCC : Quantième du mois civil.
QH : QHH : Qx :
Quantième du mois hébraïque.
QRS :
Quantième du retard solaire.
REST :
Années restant dans le dernier cycle.
RO :
Roch-Hachana de l'année courante.
ROS :
Roch-Hachana de l'année suivante.
RS
Retard solaire.
SEM :
Nombre de semaines.
TK
Numéro de la Tékoufa, de 1 à 4.
U : V: X: XX: Y : Z: Variables numériques.
D$ (1) à D$ (5) :
Diverses informations.
F$
:
Roch-Hachana, mois hébraïques.
F$ (0) : FF$ (0) : Pointillés.
F$ (1) à F$ (25)
Nom des fêtes juives.
F$ (49) à F$ (104)
Nom des Parachiyot.
FF$ (1) à FF$ (14) :
Jours particuliers.
FHS (FH)
Pointillés ou Paracha jumelée.
J$ (1) à J$ (7)
Dimanche à Chabbat.
JMC$ (1) à JMC$ (12): Jours des mois civils.
JMH$ (1) à JMH$ (13): Jours des mois hébraïques.
MC$ (1) à MC$ (12)
: Janvier à décembre.
MH$ (1) à MH$ (13) : Tichri à Eloul.
PG$ :
Programme.



;*/

/* PAGE 150

420 F$ (9) ="POURIM--
": P$ (10)="PESSA'H--
430 F$(11) ="LAG BA'OMER--
-": F$ (12) ="CHAVOU ' OT-
440 F$(13)=*JEUNE 17 TAMOUZ" :F$ (14) ="JEUNE DU 9 AV-_"
450 F$ (15)=*TOU BEAV-------" F$ (16)=" *HANOUCA---_"
460 FF$(1)="ROCH-HODECH " FF$(2)=" 'HOL-HAMOED "
470 FF$(3)= *TOU BICHVAT ":FF$(4)="LAG BA' OMER-" :
FF$ (5)="TOU BEAV---_"
480 FF$(6)="POURIM KAT. " FF$(7)="PESSAH CHENI"
490 FF$ (8)="CH.CHEKALIM** :FF$ (9)=*CH. ZAKHOR**** :
FFS (10)= "CH. PARA****** FF$ (11)=*CH. HAHODECH**
500 PFS（12）="CH.HAGADOL**"
510 FFS (13)="CH. CHOUVA**** FF$ (14) ="POURIM CHO. "
520 F$(21)=*HOCHAANA RABBA-" :F$ (22)="CHEMINI ATSERET" :
FS (23)="SIMHAT TORAH---" :F$(25)=" JEUNE 1ERS NES-"
530 F$ (51)="Berèchit**** F$ (52)="Noa h******* F$(53)=
"Lekh-Lekha** F$ (54)="Vayèra****** : F$ (55)=" 'Hayè-Sarah"
540 FS (56)="Toledot***** :E$ (57)="Vayètsè***** :F$ (58)=
"vayichla'h*"：F$（59）="Vayechev***"：F$（60）="Mikets*****m
550 F$(61)="Vayigache*** F$ (62)="Vay' hi*****" :F$ (63) =
"Chemot*****"：FS（64）="Vaera*＊****"：PS（65）="BO*★★＊★**★★"
560 F$ (66)="Bechalla'h** :F$(67)="Yitro*******;F$ (68)=
"Michpatim*** F$ (69)="Terouma***** ;F$ (70)="Tetsavé*****
570 F$(71)="Ki-Tissa**** F$ (72)="Vayakhe1**** F$ (73)=
"Pekoude****"：F$（74）="Vayikra****m: F$（75）="TSav*******n
580 F$(76)="Chemini***** :F$ (77)="Tazria'***** :F$ (78) =
"Metsora'**** F$(79)="A harè-Mot " F$ (80) ="kedochim****
590 F$(81)="Emor******** F$ (82)="Behar******* F$ (83) =
"Be 'houkotai" :F$ (84)="Bemidbar**** F$ (85)="Nasso*******
600 F$ (86)="Behaalotkha" : F$ (87)="Chelah-Leha" : F$ (88) =
"Kora'h****** F$ (89)=" Houkat***** F$ (90)="Balak*******
610 F$(91)="Pin 'has***** :F$ (92)="Mattot****** F$ (93) =
*Mass'è****** :F$ (94)="Devarim***** F$ (95)="Vaet 'hannan"
620 F$ (96)="'Ekev******* :F$ (97)="Réh******** :F$(98)=
*Choftim***** F$ (99)="Ki-Tétsè**** F$ (100)=*Ki-Tavo*****
630 F$(101)="Nitsavim***" :F$ (102)="Vayèlekh**** :F$ (103) =
*Haazinou**** F$ (104)="V.Haberakha" F$ (50)=F$ (103):
F$ (49)=F$ (102)
640 JMH (1)=30: JMH (4)=29: JMH (5)=30:JMH (6) =30:JMH (7)=29
650 JMH (8)=30: JMH (9)=29: JMH (10)=30
660 JMH (11)=29: JMH (12)=30:JMH(13)=29
670 JMC (1)=31: JMC (3)=31: JMC (4)=30: JMC (5)=31: JMC (6) =30
680 JMC (7)=31: JMC (8) =31: JMC (9)=30
690 JMC (10)=31: JMC (11)=30: JMC (12)=31

*/

/* 


★★*★
”★★★★
★★★！
・★★*
****
・★★★
・★★★★★★
PROGRAMME DE CALCUL DU CALENDRIER HEBRAIQUE
宮実角
★★★
★★★
80
90
・★*★★
★*★
100
110
INITIALISATION
120 *
130 BORDER 3: CLS
140 DIM J$(100), MC$ (100), MH$ (100), TP$ (100), D$ (100),
JMC (100), F$ (1000) , JMH(100) ,FF$(100), FHS (100)
150 JS(1)="Dimanche":JS(2) ="Lundi
": J$ (3)="Mardi
160 J$(4) ="Mercredi":JS(5)="Jeudi
" : J$ (6)="Vendredi"
170J5（7）="Chabbat "
180 MC$(1)="Janvier ":MC$(2)="Février ":MC$(3)="Mars
190 MC$ (4)="Avril ":MC$ (5) ="Mai
" : MC$ (6)=" Juin
200 MCS (7)="Juillet ":MC$(8)="Aout " MC$(9)="Septembr"
210 MC$ (10)="Octobre ": MC$(11)="Novembre"
220 MC$ (12) ="Décembre"
230 MH$(1)="Tichri ": MH$(2)="Hechvan": MH$(3)="Kislev n
240 MH$ (4)="Tévet ":MH$(5)="Chevat ": MH$(6)="Adar I "
250 MH$ (7)="Adar II" :MH$(8)="Nissan ": MH$ (9)=" Iyar
260 MH$ (10)="Sivan ":MH$(11)="Tamouz "
270 MH$ (12) ="Av
" : MH$ (13)="Eloul "
280 TP$ (1)="CONVERSION CALEND. CIVIL --> HEBRAIQUE"
290 TP$ (2)="CONVERSION CALEND. HEBRAIQUE --> CIVIL"
300 TP$ (3)="CALCUL DES 4 TEKOUFOT D'UNE ANNEE"
310 TP$(4) ="CALCUL D'UN MOLAD"
320 TP$(5)= "CYCLE LUNAIRE, SOLAIRE, CHEMITA :
TYPE DE L'ANNEE; DATE DES FETES JUIVES": PG$=" PROGRAMME "
330 TP$ (6) ="DETERMINE LES RACHE-'HODACHIM "
340 TP$ (7)="CALENDRIER CIVIL --> HEBRAIQUE*
350 TP$ (8)="CALENDRIER HEBRAIQUE --> CIVIL"
360 D$ (1)="TICHRI" : D$ (2)="TEVET" : D$ (3) ="NISSAN"
370 D$ (4)="TAMOUZ" : D$ (5)=" PRESENTER UNE DATE"
380 F$ (1)="ROCH-HACHANA---" :F$(2)="JEUNE GUEDALIA-"
390 F$ (3)="KIPPOUR-
" ; F$ (4)=" SOUCCOT---
400 F$ (5)=" ' HANOUCA-
" :F$ (6)=" JEUNE 10 TEVET-"
410 F$(7)="TOU BICHEVAT- :F$ (8)=" JEUNE D'ESTHER-"


*/
/* 

*/
