/*  PAGE 148
TP$ (1) à TP$ (8) :
X$
Titre du programme.
Touches du clavier.
QUELQUES CONSTANTES UTILISEES
1 à 7
Jours de la semaine.
1
à
12
1
à 13
Mois civils.
Mois hébraïques.
1 à 31
Jours dans le mois.
4, 100, 400
Cycles du calendrier grégorien.
7.5
7 h ½ intervalle entre 2 Tékoufot.
19, 28
28,5
Cycles lunaires et solaires.
3 X 7h 30 mn + 6 h de la montre.
30
1 et 6 h = 30 heures.
91
Intervalle entre 2 Tékoufot.
96
Mercredi 0 h = 4 X 24 h.
18,
60, 24, 7 'Halakim, minutes, heures, semaines.
106, 184, 273,295, 337, Nombre de jours.
1080
Nombre de 'halakim dans 1 heure.
1565
Avance d'un cycle lunaire.
1582,
1583,
1700, Années civiles particulières.
3760
Années d'écart entre les deux calendriers.
6480
6 h X 1080 'hal.
18
18 heures, mi-journée.
9924
9 h 204 'hal. Utiles pour fixer Roch-Hachana.
16 789
15 h 589 'hal
56 598
6 X 1 j 12 h 793 'hal = Excédent de 6 mois lunaires.
57 444
69 715
113 196
2 j 5 h 204 'hal = 1er Molad de la Création.
2 j 16 h 595 'hal = Excédent d'un cycle lunaire.
4 j 8 h 876 'hal = Excédent d'une année commune.
152 869
5 j 21 h 589 'hal = Excédent d'une année embolismique.
191 802
282
084
483
349
765
433
Avance de la 1e Tékoufa sur le Molad.
Avance d'une année commune
Avance d'une année embolismique.
29 ; 12 h 793 'hal = 1 mois lunaire.

*/
